// @flow
const findString = (haystack, needle) => haystack.indexOf(needle) > -1;
const makeArrayUnique = arr => [...new Set(arr).values()];

const findDestination = (destinations, searchTerm) =>
  searchInDestinationsByWords(destinations, searchTerm.split(' '));

const findPassion = (passions, word) => passions && passions.some(passion => findString(passion, word));

const findHotel = (destination, word) =>
  destination.city && findString(destination.city, word) ||
  findString(destination.name, word) ||
  findPassion(destination.passions, word);
const findCity = (destination, word) => findString(destination.name, word);
const findRegion = (destination, word) => findString(destination.name, word);
const findOne = word => destination => {
  if (destination.type === 'hotel') return findHotel(destination, word);
  if (destination.type === 'city') return findCity(destination, word);
  if (destination.type === 'region') return findRegion(destination, word);
  throw Error('Unhandled destination type. Should not happen!!!');
};

const searchDestinationsByWord = (destinations, word) => {
  return destinations.filter(findOne(word));
};

const searchInDestinationsByWords = (destinations, words) => {
  const found = words.reduce((result, word) => [...result, ...searchDestinationsByWord(destinations, word)], []);
  return makeArrayUnique(found);
};

module.exports = {findDestination};