class DestinationBuilder {
  build() {
    return this.destination;
  }
  addName(name) {
    this.destination.name = name;
    return this;
  }
}
class HotelBuilder extends DestinationBuilder {
  constructor() {
    super();
    this.destination = {type: HotelBuilder.TYPE, name: '', passions: [], city: ''};
  }
  addPassion(passion) {
    this.destination.passions.push(passion);
    return this;
  }
  addCity(city) {
    this.destination.city = city;
    return this;
  }
}
HotelBuilder.TYPE = 'hotel';

class CityBuilder extends DestinationBuilder {
  constructor() {
    super();
    this.destination = {type: CityBuilder.TYPE, name: ''};
  }
}
CityBuilder.TYPE = 'city';

class RegionBuilder extends DestinationBuilder {
  constructor() {
    super();
    this.destination = {type: 'region', name: ''};
  }
}
RegionBuilder.TYPE = 'region';

module.exports = {
  HotelBuilder, CityBuilder, RegionBuilder
};
