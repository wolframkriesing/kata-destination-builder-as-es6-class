This repo contains an implementation of some classes, that are built using ES6. The task is to reimplement the same structure using different other approaches, without "breaking" the class-like behaviour.

# How to use?

Install via `npm i`.
Run `npm test`.
Now go and

1) Write some tests to verify the class-y structure of the `builders.js` (e.g. some instanceof tests, verify the public interface, etc.)
1) dont use the class keyword, use `prototype`, but keep the class structure.
1) dont use class nor prototype and keep the same class structure.
